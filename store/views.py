from django.shortcuts import render,redirect
from .models import *
from django.http import JsonResponse
import json
import datetime
from .forms import sign_in,sign_up
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout

# Create your views here.

def store(request):
    if request.user.is_authenticated:
        customer = request.user.customer
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = order.cartitem_set.all()
        cartItems = order.get_cart_items
    else:
        items = []
        order = {'get_cart_total':0, 'get_cart_items':0}
        cartItems = order['get_cart_items']

    products = Product.objects.all()
    context = {'products':products, 'cartItems':cartItems}
    return render(request, 'store/store.html', context)

def cart(request):
    if request.user.is_authenticated:
        customer = request.user.customer
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = order.cartitem_set.all()
        cartItems = order.get_cart_items
    else:
        items = []
        order = {'get_cart_total':0, 'get_cart_items':0, 'cartItems':cartItems}
        cartItems = order['get_cart_items']

    context = {'items':items, 'order':order, 'cartItems':cartItems}
    return render(request, 'store/cart.html', context)

def checkout(request):
    if request.user.is_authenticated:
        customer = request.user.customer
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        items = order.cartitem_set.all()
        cartItems = order.get_cart_items
    else:
        items = []
        order = {'get_cart_total':0, 'get_cart_items':0}
        cartItems = order['get_cart_items']
        
    context = {'items':items, 'order':order, 'cartItems':cartItems}
    return render(request, 'store/checkout.html', context)

def review(request):
    context = {}
    return render(request, 'store/review.html', context)

def updateItem(request):
    data = json.loads(request.body)
    productId = data['productId']
    action = data['action']

    print('productId:', productId)
    print('Action:', action)
    customer = request.user.customer
    product = Product.objects.get(id=productId)
    order, created = Order.objects.get_or_create(customer=customer, complete=False)

    cartitems, created = CartItem.objects.get_or_create(order=order, product=product)
    if action == 'add':
        cartitems.quantity = (cartitems.quantity + 1)
    elif action == 'remove':
        cartitems.quantity = (cartitems.quantity - 1)
    cartitems.save()
    if cartitems.quantity <= 0:
        cartitems.delete()
    
    return JsonResponse('Item was added', safe=False)

def processOrder(request):
    transaction_id = datetime.datetime.now().timestamp()
    data = json.loads(request.body) 
    if request.user.is_authenticated:
        customer = request.user.customer
        order, created = Order.objects.get_or_create(customer=customer, complete=False)
        total = data['form']['total']
        order.transaction_id = transaction_id

        if total == order.get_cart_total:
            order.complete = True
        else:
            order.complete = True
        order.save()
    else:
        print('user not logged in')
    return JsonResponse('Transaction Completed', safe=False)
def review(request):
    data = Product.objects.all()
    context = {'data':data}
    return render(request, 'store/main_rev.html',context)
def reviews(request,pk):
    data= Product.objects.filter(pk=pk)
    if request.method=="POST":
        name = request.POST['name']
        user = request.POST['user']
        feedback = request.POST['feedback']
        rev_data = Review.objects.create(product=name,reviewer=user,review=feedback)
        x= Review.objects.all()
        return render(request,'store/review.html',{'data':data,'review':x})
    else:
        x= Review.objects.all()
        return render(request,'store/review.html',{'data':data,'review':x})
def history(request):
    data = Order.objects.all()
    print(data)
    return render(request,'store/history.html',{'data':data})

def log_in(request):
    if request.method=="POST":
        fillform = sign_in(request.POST)
        if(fillform.is_valid()):
            user_name= fillform.cleaned_data["Username"]
            password = fillform.cleaned_data["Password"]
            SignIn = authenticate(request, username=user_name,password=password)
            if SignIn is not None :

                login(request,SignIn)
                return redirect('/')
            else:
                fillform = sign_in()
                message='Invalid username, Please Try again'
                return render(request,'store/login.html',{'form':fillform,'message':message})
    else:
        fillform=sign_in()
        return render(request,'store/login.html',{'form':fillform})

def signup(request):
    if request.method =="POST":
        fillform= sign_up(request.POST)
        if (fillform.is_valid()):
            user_name =fillform.cleaned_data["username"]
            email = fillform.cleaned_data["email"]
            password = fillform.cleaned_data["password"]
            first_name1= fillform.cleaned_data["Firstname"]
            last_name1 = fillform.cleaned_data["Lastname"]
            try:
                User.objects.get(username=user_name)
                return redirect("/signup/")
            except:
                x= User.objects.create_user(user_name,email,password)
                x.last_name =last_name1
                x.first_name =first_name1
                x.save()
                SignIn = authenticate(request, username=user_name,password=password)
                login(request,SignIn)

                y= Customer.objects.create(user=request.user,name=first_name1)
                y.save()
                print(y)
                return redirect('/')
    else:
        fillform=sign_up()
        return render(request,'store/signup.html',{'form':fillform})
def signout(request):
    logout(request)
    return redirect('/')
